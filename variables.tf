variable "proxmox-ip" {
  type = string
  default = ""
}

variable "proxmox-user" {
  type = string
  default = ""
}

variable "proxmox-password" {
  type = string
  default = ""
}

variable "vm-name" {
  type = string
  default = ""
}

variable "vm-template" {
  type = string
  default = ""
}

variable "vm-cpu" {
  type = string
  default = ""
}

variable "vm-ram" {
  type = string
  default = ""
}

variable "vm-disk" {
  type = string
  default = ""
}
